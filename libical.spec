Name:           libical
Version:        3.0.19
Release:        1
Summary:        An Open Source implementation of the iCalendar protocols and protocol data formats.
License:        LGPL-2.0-only or MPL-2.0
URL:            https://libical.github.io/libical/
Source0:        https://github.com/%{name}/%{name}/releases/download/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc-c++ make
BuildRequires:  cmake >= 3.20.0
BuildRequires:  gtk-doc perl-interpreter vala
BuildRequires:  python3 python3-pip python3-gobject libxslt
BuildRequires:  pkgconfig(icu-uc) pkgconfig(gobject-2.0) pkgconfig(gobject-introspection-1.0) pkgconfig(libxml-2.0) pkgconfig(icu-i18n)
Requires:       tzdata
Provides:       libical-glib = %{version}-%{release}
Obsoletes:      libical-glib < %{version}-%{release}

Patch0: 	libical-bugfix-timeout-found-by-fuzzer.patch

%description
Libical is an open source implementation of the IETF's iCalendar calendaring
and scheduling protocols (RFC 2445, 2446, and 2447).
It parses iCal components and provides a C API for manipulating
the component properties, parameters, and subcomponents.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Provides:       libical-glib-doc = %{version}-%{release}
Provides:	libical-glib-devel = %{version}-%{release} 
Obsoletes:      libical-glib-doc < %{version}-%{release}
Obsoletes:	libical-glib-devel < %{version}-%{release}

%description    devel
Contains libraries and header files for libical.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%{cmake} \
  -DUSE_INTEROPERABLE_VTIMEZONES:BOOL=true \
  -DICAL_ALLOW_EMPTY_PROPERTIES:BOOL=true \
  -DGOBJECT_INTROSPECTION:BOOL=true \
  -DICAL_GLIB:BOOL=true \
  -DICAL_GLIB_VAPI:BOOL=true \
  -DSHARED_ONLY:BOOL=true

%cmake_build -j1

%install
%cmake_install

# This is just a private build tool, not meant to be installed
rm %{buildroot}/%{_libexecdir}/libical/ical-glib-src-generator

%check
%ctest -j1

%files
%doc README.md THANKS
%license LICENSE LICENSE.LGPL21.txt LICENSE.MPL2.txt
%{_libdir}/*.so.3*
%{_libdir}/girepository-1.0/*.typelib
%{_datadir}/gir-1.0/*.gir

%files devel
%doc doc/UsingLibical.md
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/cmake/LibIcal/
%{_includedir}/libical/
%{_includedir}/libical-glib/
%{_datadir}/vala/vapi/libical-glib.vapi
%{_datadir}/gtk-doc/html/%{name}-glib

%changelog
* Tue Dec 24 2024 Funda Wang <fundawang@yeah.net> - 3.0.19-1
- update to 3.0.19

* Thu Nov 07 2024 Funda Wang <fundawang@yeah.net> - 3.0.18-3
- adopt to new cmake macro

* Fri Aug 16 2024 zhangxingrong <zhangxingrong@uniontech.cn> - 3.0.18-2
- icalcomponent.c - avoid crashing in icalcomponent_normalize
- icalcomponent.c - fix a memleak in icalcomponent_normalize()

* Mon Jul 29 2024 dillon chen <dillon.chen@gmail.com> - 3.0.18-1
- Type:requirements
- Id:NA
- SUG:NA
- DESC:update libical version  to 3.0.18

* Thu Dec 28 2023 yanglu <yanglu72@h-partners.com> - 3.0.17-1
- Type:requirements
- Id:NA
- SUG:NA
- DESC:update libical version  to 3.0.17

* Thu Feb 02 2023 wangjunqi <wangjunqi@kylinos.cn> - 3.0.16-1
- update version to 3.0.16

* Tue Dec 14 2021 xihaochen <xihaochen@huawei.com> - 3.0.11-1
- Type:requirements
- Id:NA
- SUG:NA
- DESC:update libical to 3.0.11

* Wed Jul 22 2020 zhangxingliang <zhangxingliang3@huawei.com> - 3.0.8-1
- Type:update
- Id:NA
- SUG:NA
- DESC:update to 3.0.8

* Sun Jun 28 2020 hanzhijun <hanzhijun1@huawei.com> - 3.0.4-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix build failed

* Tue Mar 10 2020 songnannan <songnannan2@huawei.com> - 3.0.4-2
- bugfix in oss-fuzz

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.0.4-1
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:updtae to 3.0.4

* Thu Dec 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.0.3-9
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add require tzdata

* Sat Sep 14 2019 chenzhenyu <chenzhenyu13@huawei.com> - 3.0.3-8
- Package init
